import { Injectable, ComponentFactoryResolver, Injector, ApplicationRef, NgZone } from '@angular/core';

import { SpotService } from './spot.service';
import { Spot } from '../models/spot';
import { SpotDetailsComponent } from '../components/dashboard/spot-details/spot-details.component';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { MatDialog } from '@angular/material';
import { FilterSpotsComponent } from '../components/shared/filter-spots/filter-spots.component';

declare let L; //leaflet

@Injectable({
  providedIn: 'root'
})
export class MapService {

  map: any;
  yellowIcon = L.icon({
    iconUrl: 'assets/favorite-marker.png',
    iconSize: [32, 32],
    iconAnchor: [22, 31],
    popupAnchor: [-3, -21]
  });

  compRef: any;

  constructor(private spotService: SpotService,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef,
    private dialog: MatDialog,
    private zone: NgZone
  ) { }

  getMap() {
    return this.map
  }

  addMap() {
    this.map = L.map('map').setView([0, 0], 2);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    let zone = this.zone;
    let addFilter = this.addFilter;
    let me = this;
    let filterButton = L.Control.extend({
      options: {
        position: 'topright'
      },
      onAdd: function (map) {
        var div = L.DomUtil.create('div', 'myContianer');
        div.innerHTML = '<button class="btn btn-info" style="font-size: 0.8rem;"><i class="fas fa-filter"></i> Filters</button>';
        L.DomEvent.on(div, "click", (e) => {
          zone.run(() => {
            addFilter(e.path[0],me);
          })
        });
        return div;
      }
    });
    this.map.addControl(new filterButton())
  }

  addMarkers(country?: string, windProbability?: number) {
    this.spotService
      .getAllSpots(country, windProbability)
      .subscribe(
        data => {
          let spots: Spot[] = data.result;
          spots.forEach(elem => {
            if (elem.isFavorite) {
              let marker = L.marker([elem.latitude, elem.longitude], { icon: this.yellowIcon }).addTo(this.map).bindPopup(() => this.createPopupSpotDetails(elem, marker, this.map));
            } else {
              let marker = L.marker([elem.latitude, elem.longitude]).addTo(this.map).bindPopup(() => this.createPopupSpotDetails(elem, marker, this.map));
            }
          });
        }
      )
  }

  addMarker(elem: Spot) {
    let marker = L.marker([elem.latitude, elem.longitude]).addTo(this.map).bindPopup(() => this.createPopupSpotDetails(elem, marker, this.map));
  }

  removeMarkers() {
    this.map.eachLayer((layer) => {
      if (layer instanceof L.Marker) {
        this.map.removeLayer(layer);
      }
    })
  }

  private createPopupSpotDetails(spot: Spot, marker: any, map: any) {
    if (this.compRef) {
      this.compRef.destroy();
    }
    const compFactory = this.resolver.resolveComponentFactory(SpotDetailsComponent);
    this.compRef = compFactory.create(this.injector);
    this.compRef.instance.spot = spot; // foarte important setarea parametrului pe instanta creata :)  

    this.compRef.instance.refreshMap.subscribe((param) => {
      map.removeLayer(marker);
      spot.isFavorite = param.isFavorite
      if (param.isFavorite) {
        let marker = L.marker([spot.latitude, spot.longitude], { icon: this.yellowIcon }).addTo(map).bindPopup(() => this.createPopupSpotDetails(spot, marker, map));
      } else if (!param.isDelete) { // is not deleted and remove from favorite
        let marker = L.marker([spot.latitude, spot.longitude]).addTo(map).bindPopup(() => this.createPopupSpotDetails(spot, marker, map));
      }
    })

    if (this.appRef.attachView) {
      this.appRef.attachView(this.compRef.hostView);
      this.compRef.onDestroy(() => {
        this.appRef.detachView(this.compRef.hostView);
      });
    } else {
      this.appRef['registerChangeDetector'](this.compRef.changeDetectorRef);
      this.compRef.onDestroy(() => {
        this.appRef['unregisterChangeDetector'](this.compRef.changeDetectorRef);
      });
    }
    let div = document.createElement('div');
    div.appendChild(this.compRef.location.nativeElement);
    return div;
  }

  private addFilter(ele: any,me:any) {
    console.log(me);
    function GetScreenCordinates(obj) {
      var p = { x:0, y: 0 };
      p.x = obj.offsetLeft;
      p.y = obj.offsetTop;
      while (obj.offsetParent) {
        p.x = p.x + obj.offsetParent.offsetLeft;
        p.y = p.y + obj.offsetParent.offsetTop;
        if (obj == document.getElementsByTagName("body")[0]) {
          break;
        }
        else {
          obj = obj.offsetParent;
        }
      }
      return p;
    }
    var p = GetScreenCordinates(ele);
    console.log(p);
    const dialogRef = me.dialog.open(FilterSpotsComponent, {
      width: '180px',
      position:{top:p.y+'px',left:p.x-100+'px'},
      data: {name:"",windProb:null}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        me.removeMarkers();
        me.addMarkers(result.name,result.windProb);
      }
    });
  }
}


