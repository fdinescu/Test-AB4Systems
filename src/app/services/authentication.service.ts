//authentication.services.ts
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';

const httpOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private BASE_URL = 'https://ab4-kitesurfing.herokuapp.com';

  constructor(private http: HttpClient) { }

  getToken(): string {
    return localStorage.getItem('token');
  }

  login(email: string, password: string): Observable<any> {
    const url = `${this.BASE_URL}/api-user-log-in`;
    return this.http.post<User>(url, JSON.stringify({ email: email, password: password }), httpOption);
  }
  
  signUp(email: string, password: string): Observable<any> {
    const url = `${this.BASE_URL}/api-user-sign-up`;
    return this.http.post<User>(url,JSON.stringify({email, password}), httpOption);
  }
}
