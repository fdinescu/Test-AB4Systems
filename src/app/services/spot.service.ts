import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Spot } from '../models/spot';

const httpOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class SpotService {

  private BASE_URL = 'https://ab4-kitesurfing.herokuapp.com';

  constructor(private httpClient: HttpClient) { }

  getPaginatedSpots(page: number): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-get-some`;
    return this.httpClient.post<any>(url, JSON.stringify({ skip: page * 5, count: 5 }), httpOption);
  }

  getAllSpots(country?: string, windProbability?: number): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-get-all`;
    return this.httpClient.post<any>(url, JSON.stringify({ country: country, windProbability: windProbability }), httpOption);
  }

  addFavorites(spotId: string): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-favorites-add`;
    return this.httpClient.post<any>(url, JSON.stringify({ spotId: spotId }), httpOption);
  }

  removeFavorites(spotId: string): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-favorites-remove`;
    return this.httpClient.post<any>(url, JSON.stringify({ spotId: spotId }), httpOption);
  }

  addSpot(spot: Spot): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-add`;
    return this.httpClient.post<any>(url, JSON.stringify(spot), httpOption);
  }

  removeSpot(spotId: string): Observable<any> {
    let url = `${this.BASE_URL}/api-spot-remove`;
    return this.httpClient.post<any>(url, JSON.stringify({ spotId: spotId }), httpOption);
  }
}
