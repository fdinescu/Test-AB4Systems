export class Spot {
    id: string;
    name: string;
    longitude: number;
    latitude: number;
    windProbability: number;
    country: string;
    whenToGo: string;
    isFavorite: boolean;
}
