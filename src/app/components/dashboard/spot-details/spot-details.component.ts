import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Spot } from 'src/app/models/spot';
import { Subject } from 'rxjs';
import { SpotService } from 'src/app/services/spot.service';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-spot-details',
  templateUrl: './spot-details.component.html',
  styleUrls: ['./spot-details.component.css']
})
export class SpotDetailsComponent implements OnInit {

  @Input() spot: Spot;
  @Output() refreshMap:Subject<any> = new Subject();

  math = Math;

  constructor(private spotService:SpotService,private dialog: MatDialog) { }

  ngOnInit() {
  }

  addFavorites(spot:Spot){
    this.spotService.addFavorites(spot.id).subscribe((data)=>{
      this.refreshMap.next({isFavorite:true,isDelete:false});
    })
    
  }

  removeFavorites(spot:Spot){
    this.spotService.removeFavorites(spot.id).subscribe((data)=>{
      this.refreshMap.next({isFavorite:false,isDelete:false});
    })
  }
  
  removeSpot(spot:Spot){
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: "Do you wish to delete this spot ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.spotService.removeSpot(spot.id).subscribe((data)=>{
          this.refreshMap.next({isFavorite:false,isDelete:true});
        })
      } 
    });
  }

}
