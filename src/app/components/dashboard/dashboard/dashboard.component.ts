import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { merge, of } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';

import { SpotService } from 'src/app/services/spot.service';
import { Spot } from 'src/app/models/spot';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'country', 'longitude', 'latitude', 'windProbability', 'whenToGo'];
  data: Spot[] = [];

  resultsLength = 0;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private spotService: SpotService,private mapService:MapService) { }

  ngOnInit() {
    this.mapService.addMap();
    this.mapService.addMarkers();
  }

  ngAfterViewInit() {
    //sortare nu am api care sa trimit campul dupa care sa faca sortarea si nici ordinea ASC sau DESC :(
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.spotService!.getPaginatedSpots(this.paginator.pageIndex);
        }),
        map(data => {
          this.resultsLength = 500; // nu am in api numar total de elemente :( 
          return data.result;
        }),
        catchError(() => {
          return of([]);
        })
      ).subscribe(data => this.data = data);
  }

}
