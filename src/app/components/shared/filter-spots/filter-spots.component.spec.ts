import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSpotsComponent } from './filter-spots.component';

describe('FilterSpotsComponent', () => {
  let component: FilterSpotsComponent;
  let fixture: ComponentFixture<FilterSpotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterSpotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSpotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
