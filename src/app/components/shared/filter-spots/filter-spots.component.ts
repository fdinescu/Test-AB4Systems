import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-filter-spots',
  templateUrl: './filter-spots.component.html',
  styleUrls: ['./filter-spots.component.css']
})
export class FilterSpotsComponent implements OnInit {

  constructor( 
    public dialogRef: MatDialogRef<FilterSpotsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {name:string,windProb:number}) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
