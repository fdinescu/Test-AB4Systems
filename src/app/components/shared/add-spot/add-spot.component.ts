import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { Spot } from 'src/app/models/spot';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { SpotService } from 'src/app/services/spot.service';

declare let L; //leaflet
@Component({
  selector: 'app-add-spot',
  templateUrl: './add-spot.component.html',
  styleUrls: ['./add-spot.component.css']
})
export class AddSpotComponent implements OnInit {

  yellowIcon = L.icon({
    iconUrl: 'assets/favorite-marker.png',
    iconSize: [32, 32],
    iconAnchor: [22, 31],
    popupAnchor: [-3, -21]
  });

  btdAddSpotDisabled = true;

  constructor(
    private dialog: MatDialog,
    private ngZone: NgZone,
    private spotService: SpotService,
    public dialogRef: MatDialogRef<AddSpotComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Spot) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    const map = L.map('mapAdd').setView([0, 0], 2);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    this.spotService
      .getAllSpots()
      .subscribe(
        data => {
          let spots: Spot[] = data.result;
          spots.forEach(elem => {
            if (elem.isFavorite) {
              L.marker([elem.latitude, elem.longitude], { icon: this.yellowIcon }).addTo(map);
            } else {
              L.marker([elem.latitude, elem.longitude]).addTo(map);
            }
          });
        }
      )


    map.on('click', (ev) => {
      const latlng = map.mouseEventToLatLng(ev.originalEvent);
      this.ngZone.run(() => {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
          width: '350px',
          data: "Do you wish to add spot on this location?"
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.btdAddSpotDisabled = false;
            this.data.latitude = latlng.lat;
            this.data.longitude = latlng.lng;
            this.data.windProbability = 90;
            L.marker([latlng.lat, latlng.lng]).addTo(map);
          }
        });
      });
    });
  }
}
