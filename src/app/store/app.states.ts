import { createFeatureSelector } from '@ngrx/store';

import * as authentication from './reducers/authentication.reducer';

export interface AppState {
  authenticationState: authentication.State;
}

export const reducers = {
  auth: authentication.reducer
};

export const selectAuthState = createFeatureSelector<AppState>('auth');