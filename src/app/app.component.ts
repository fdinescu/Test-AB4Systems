import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material';

import { AppState, selectAuthState } from './store/app.states';
import { Logout } from './store/actions/authentication.actions';
import { SpotService } from './services/spot.service';
import { AddSpotComponent } from './components/shared/add-spot/add-spot.component';
import { Spot } from './models/spot';
import { MapService } from './services/map.service';

declare let L;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Test AB4Systems';
  getState: Observable<any>;
  isAuthenticated: false;
  user = null;
  errorMessage = null;
  spotsSubscription: any;

  constructor(
    private dialog: MatDialog,
    private spotService: SpotService,
    private mapService: MapService,
    private store: Store<AppState>
  ) {
    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit() {
    this.getState.subscribe((state) => {
      this.isAuthenticated = state.isAuthenticated;
      this.user = state.user;
      this.errorMessage = state.errorMessage;
    });
  }

  logOut(): void {
    this.store.dispatch(new Logout);
  }

  addSpot() {
    const dialogRef = this.dialog.open(AddSpotComponent, {
      width: '350px',
      data: new Spot()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let whentoGo = result.whenToGo.toLocaleString('en-us', { month: 'long' });
        result.whenToGo = whentoGo;
        this.spotsSubscription = this.spotService
          .addSpot(result)
          .subscribe(
            data => {
              let spot: Spot = data.result;
              this.mapService.addMarker(spot);
            });
      }
    });
  }
}
