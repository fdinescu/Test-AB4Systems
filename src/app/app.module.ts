import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule, MatProgressSpinnerModule, MatPaginatorModule, MatDialogModule,MatDatepickerModule,MatInputModule,MatNativeDateModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/users/login/login.component';

import { AuthEffects } from './store/effects/authentication.effects';
import { AuthenticationService } from './services/authentication.service';
import { reducers } from './store/app.states';
import { TokenInterceptor } from './services/token.interceptor';
import { AuthGuard } from './services/auth-guard.service';
import { SignUpComponent } from './components/users/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard/dashboard.component';
import { SpotDetailsComponent } from './components/dashboard/spot-details/spot-details.component';
import { ConfirmationDialogComponent } from './components/shared/confirmation-dialog/confirmation-dialog.component';
import { AddSpotComponent } from './components/shared/add-spot/add-spot.component';
import { FilterSpotsComponent } from './components/shared/filter-spots/filter-spots.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    DashboardComponent,
    SpotDetailsComponent,
    ConfirmationDialogComponent,
    AddSpotComponent,
    FilterSpotsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([AuthEffects]),
    AppRoutingModule
  ],
  entryComponents: [AddSpotComponent,SpotDetailsComponent,ConfirmationDialogComponent,FilterSpotsComponent],
  providers: [
    AuthenticationService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {}
}
